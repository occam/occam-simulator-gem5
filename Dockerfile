# DOCKER-VERSION 0.11.1
FROM occam/mjqxgzi--ovrhk3tuouwweyltmuwtembrgqwtanq-
ADD . /simulator
RUN apt-get update
RUN echo 'y' | apt-get install swig
RUN echo 'y' | apt-get install m4
RUN hg clone http://repo.gem5.org/gem5-stable /simulator/package
RUN cd /simulator; /bin/bash -c 'source /occam/setup_python.sh; source /occam/setup_perl.sh; cd /simulator; bash /simulator/build.sh'
CMD ['/bin/bash']
