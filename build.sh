cd package

pyenv local 2.7.6
pip install --egg SCons
pyenv rehash

cp ../SConstruct.patch .
patch < SConstruct.patch

scons build/X86/gem5.opt
